from django.shortcuts import render
from django.http import JsonResponse
from django.http.response import HttpResponseNotAllowed

from django.utils.safestring import mark_safe

import re

from Crypto.PublicKey import RSA

# Create your views here.



def index(request):
    return render(request, 'index.html', {'users':Autoridad.registrados})



class Autoridad:

    registrados = {}

    def gen_keys():
        return RSA.generate(bits=1024)

    def registrar(request):
        if request.method != "POST":
            return HttpResponseNotAllowed(['POST'])

        print(request.POST.items())
        user = request.POST['u']

        if not user:
            data = {'status':'fallo'}
            return JsonResponse(status=400, data=data)

        if user in Autoridad.registrados:
            data = {'status':'fallo', 'mensaje':'Usuario ya registrado.'}
            return JsonResponse(status=500, data=data)
        
        user = re.sub(r'\s', '', user)
        keys = Autoridad.gen_keys()
        priv_key = keys.exportKey().decode('utf8')
        pub_key = keys.publickey().exportKey().decode('utf8')

        Autoridad.registrados[user] = {'pub_key':pub_key, 'priv_key':priv_key}

        data = {'status':'ok', 'priv_key':priv_key}
        return JsonResponse(data=data)



    def query(request):
        if request.method != "POST":
            return HttpResponseNotAllowed(['POST'])

        user = request.POST['u']

        if not user:
            data = {'status':'fallo'}
            return JsonResponse(status=500, data=data)

        if user not in Autoridad.registrados:
            data = {'status':'fallo', 'mensaje':'Usuario no registrado.'}
            return JsonResponse(status=404, data=data)

        x = {
        'name':user,
        'pub_key':Autoridad.registrados[user]['pub_key'],
        'priv_key':Autoridad.registrados[user]['priv_key']}

        return JsonResponse(data=x)


    def getUsers(request):
        if request.method != "GET":
            return HttpResponseNotAllowed(['GET'])

        if len(Autoridad.registrados) > 0:
            return JsonResponse([(x) \
                for x in Autoridad.registrados.keys()], safe=False)
        else:
            return JsonResponse(status=500, data={
                'status':'fallo',
                'mensaje':'No hay usuarios registrados.'})        

