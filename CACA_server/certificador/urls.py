from django.conf.urls import url

from .views import Autoridad
from .views import index

urlpatterns = [
    url(r'^$', index),
    url(r'^index/$', index, name='index'),
    url(r'^signup/$', Autoridad.registrar , name='signup'),
    url(r'^consultar/$', Autoridad.query, name='consultar'),
    url(r'^usuarios/$', Autoridad.getUsers, name='usuarios'),
]