# Cifrado Asimétrico Certificado por Autoridad
El *Cifrado Asimétrico Certificado por Autoridad* (**CACA**) es el nombre del proyecto final de la materia de **Criptografía: Técnicas y Algoritmos** de la **Benemérita Universidad Autónoma de Zacatecas** del semestre Enero-Junio 2019. Desarrollado en Python con el framework Django.

## Descarga y ejecución.
El proyecto se puede obtener descargándolo desde el repositorio de [GitLab](https://gitlab.com/puerkito66/CACA) o clonandolo en un directorio con el comando:

    git clone https://gitlab.com/puerkito66/CACA.git

Para poder usarlo es necesario tener **Python 3.7** o superior y el gestor de paquetes **Pip 1.8** o superior.
Para instalar las dependencias necesarias se necesita instalar el servidor Redis (binario para Windows incluido en este repositorio).
Una vez instalado el servidor, para instalar las dependecias necesarias ejecuta el comando:

    python -m pip install -r requeriments.txt

En caso de Windows, `Twisted` no cuenta con distribución oficial para la plataforma, por lo que se ofrece en este proyecto un fork funcional. Hay que instalarlo con el comando:

    python -m pip install Twisted-19.2.0-win64.whl

Una vez que las dependecias han sido instaladas, se pone en marcha primero el servidor (proyecto `CACA_server`) con el comando:

    python manage.py runserver 0.0.0.0:8080

Para el cliente, el en archivo `settings.py` (proyecto `CACA_chat`) hay que editar la variable `AUTORIDAD_CERTIFICADORA` con la dirección del servidor.
Con la dirección del servidor actualizada, en una nueva terminal se ejecuta el comando:

    python manage.py runserver

Y nuestro cliente estará listo para accesarse desde el navegador en la dirección:

    [http://locahost:8000/chat/](http://127.0.0.1:8000/chat/)



## Colaboradores.

Este proyecto fue realizado por los alumnos del 8° semestre de la carrera de **Ingeniería de Software**:

 - [Orlando Rodríguez Hernández](https://gitlab.com/puerkito66)
 - [Felipe de Jesús Ulloa Ontiveros](https://gitlab.com/felipeulloa)

