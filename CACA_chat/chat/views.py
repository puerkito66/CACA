from django.shortcuts import render, redirect
from django.utils.safestring import mark_safe
from django.http.response import HttpResponse
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt


import json

import requests as reqs

from django.conf import settings

# Create your views here.

autoridad = settings.AUTORIDAD_CERTIFICADORA



def index(request):
    return render(request, 'chat/index.html', {'autoridad':autoridad})



def room(request, remitente, destinatario):

    rem = reqs.post(autoridad + "api/consultar/", data={'u':remitente})
    dest = reqs.post(autoridad + "api/consultar/", data={'u':destinatario})

    # Un usuario no puede chatear consigo mismo
    if remitente == destinatario:
        return HttpResponse(status=500)

    # Si el servidor no responde que están registrados
    if rem.status_code != 200 and dest.status_code == 200:
        return render(request, 'chat/404.html', status=404)

    # Se define el nombre del canal de escucha y el canal de envío
    sender_channel = remitente + destinatario
    listening_channel = destinatario + remitente

    # Se obtienen las llaves de cada uno
    # La llave privada del que inicia la conversación.
    # La llava pública del remitente
    my_priv_key = json.loads(rem.text)['priv_key']
    the_pub_key = json.loads(dest.text)['pub_key']

    return render(request, 'chat/room.html', {
        'user_name_json': destinatario,
        'remitente':remitente,
        'the_pub_key':the_pub_key,
        'my_priv_key':my_priv_key,
        'sender_channel':mark_safe(json.dumps(sender_channel)),
        'listening_channel':mark_safe(json.dumps(listening_channel))
    })

@require_POST
@csrf_exempt
def registro(request):
    username = request.POST['username']

    if not username:
        return HttpResponse(status=500)

    r = reqs.post(autoridad + "api/signup/", data={'u':username})

    if r.status_code == 400:
        return JsonResponse(status=400, data={
            'status':'fallo',
            'mensaje':'Solicitud de registro vacía.'})

    if r.status_code == 500:
        return JsonResponse(status=500, data={
            'status':'fallo',
            'mensaje':'Usuario ya registrado.'
            })

    key = json.loads(r.text)['priv_key']

    return ('chatroom/', {
        'name':mark_safe(json.dumps(username)),
        'key':key
    })


def listado(request, username):

    users = reqs.get(
        autoridad + 'api/usuarios/')

    priv_key = reqs.post(
        autoridad + 'api/consultar/',
        data={
            'u':username
    })

    if users.status_code != 200 or priv_key.status_code != 200:
        return render(request, 'chat/404.html', status=404)

    users = json.loads(users.text)
    priv_key = json.loads(priv_key.text)


    return render(request, 'chat/users_list.html',{
        'username':username,
        'users':users,
        'priv_key':priv_key
        })

    




