from django.conf.urls import url

from .views import index, room, registro, listado

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^signup$', registro, name='registro'),
    url(r'^chatroom/(?P<username>[^/]+)/$', listado, name='listado'),
    url(r'^from-(?P<remitente>[^/]+)-2-(?P<destinatario>[^/]+)/$', room, name='room'),
]
